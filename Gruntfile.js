module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'src/css/main.css': 'src/css/sass/styles.scss',
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			},
			js: {
				files: ['src/js/*.js'],
				tasks: ['uglify']
			}
		},
		uglify: {
			js: {
				files: {
					'src/js/js.min.js': ['src/js/vendor/bootstrap.js', 'src/js/js.js']
				}
			}
		},
		autoprefixer: {
            options: {
                // We need to `freeze` browsers versions for testing purposes.
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },

            single_file: {
                src: 'src/css/main.css',
                dest: 'src/css/main.min.css'
            },
        },
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.registerTask('default',['watch', 'autoprefixer']);

};